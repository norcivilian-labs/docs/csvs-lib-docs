# Tutorial

Let's nodejs query, update, delete

Install from the NPM registry
```shell
yarn install @fetsorn/csvs-js
```

Import the library
```js
import { CSVS } from @fetsorn/csvs-js;
```

Clone a sample csvs dataset
```shell
git clone https://gitlab.com/norcivilian-labs/csvs-template-en
```

Point the dir variable to the dataset. In NodeJS, that would be the path to a dataset directory.
```js
import fs from 'fs';

const dir = "/path/to/csvs-template-en"
```

And query the records
```js
const records = await new CSVS({ fs, dir }).select(new URLSearchParams());
// [{
//   _: 'datum',
//   UUID: '...',
//   datum: 'value1',
//   actdate: '2001-01-01'
// }]
```

To edit a record, change its json and pass it back to csvs
```js
const recordNew = { datum: 'value2', ...records[0] }

await new CSVS({ fs, dir }).update(recordNew);
```

To add a new record, pass a json object without a UUID to `CSVS.update(record)`. The function will return the new record with a generated UUID.
```js
const recordNew = await new CSVS({ fs, dir }).update({ 
  _: 'datum',
  datum: 'value2',
  actdate: '2003-03-03'
});
```

To delete an record, pass it to `CSVS.delete(record)` and the library will remove record UUID from the dataset.
```js
await new CSVS({ fs, dir }).delete(record);
```

Learn more about csvs in the [User Guides](./user_guides.md).
