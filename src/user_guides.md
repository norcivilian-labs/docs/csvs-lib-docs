# User Guides

- [Running search queries](./01_running_search_queries.md)
- [Cloning from git](./02_cloning_from_git.md)
- [Editing dataset schema](./03_editing_dataset_schema.md)
- [Adding a family tree](./04_adding_a_family_tree.md)
- [Caching a hard drive](./05_caching_a_hard_drive.md)
- [Publishing to social media](./06_publishing_to_social_media.md)
- [Merging datasets](./07_merging_datasets.md)
  
To learn more about the architecture of evenor, see [Design](./design.md) and [Requirements](./requirements.md).

