# Delete

delete literal must take literal query object notation and delete records that match literal constraints

delete regular expression must take regular expression query object notation and delete records that match regular expression constraint

should remove literal and only match on regular expression?

WON'T delete unxpected extra records that match constraint by accident

should we only have literal match after query to avoid unexpected deletions?

To learn more about the architecture of evenor, see [Design](./design.md) and [Requirements](./requirements.md).
