# Line

```pdl
if select
    if querying 
        assign complete to current without trait
    else
        assign complete to current
    if eof 
        if tablet is not eager or if eager matched
            return complete
        else
            return empty
    if empty line 
        return empty
    parse line
    if key matches first
        assign first is new false
    else
        assign first is new true
    if eager and first is new 
        call step with initial
        assign current
    else
        call step with current
        assign current
    if eager and first is not new and is match or is previous match 
        assign is match true TODO: unravel this
    else 
        assign is match false
    if eager and first is new and previous is match
        return initial, current, first, is match, complete
    else
        return initial, current, first, is match
else if update
    if eof
        call step
    if empty line
        return empty state
    parse line
    if key matches first
        assign key is new false
    else 
        assign key is new true
    if key is new
        call step
    else
        assign empty state
    if new relations has key TODO: remove relations and make sense of match partial
        return lines
else if delete
    if trait is first and first matches trait
        return true
    if trait is second and second matches trait
        return true
    else
        return false
else if insert
    return key,value
else 
    panic
```
