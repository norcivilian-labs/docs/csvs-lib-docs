# CSVS in the Browser

Csvs-lib changes a csvs dataset by passing filepaths to the methods on an FS interface. The simplest way is to import from the `fs` Node module to interact with local filesystem. 
The browser is barred from filesystem access, but can emulate it using [LightningFS](https://github.com/isomorphic-git/lightning-fs), which stores data in [IndexedDB](https://developer.mozilla.org/en-US/docs/Web/API/IndexedDB_API).

> See [BrowserFS](https://github.com/jvilk/BrowserFS) and [filer](https://github.com/filerjs/filer) for other examples of emulating a filesystem in the browser. 

First, initialize the FS instance. The name 'virtualfs' is used to determine the IndexedDb store name. It is recommended to only create one of these instances for the entire lifetime of the application.
```js
const fs = new LightningFS('virtualfs');
```

Now, let's populate the filesystem with a dataset by cloning it with [isomorphic-git](https://isomorphic-git.org/).
```js
const { clone } = await import('isomorphic-git');

const http = await import('isomorphic-git/http/web/index.cjs');

const dir = '/csvs-template-en';

const url = 'https://gitlab.com/norcivilian-labs/csvs-template-en';

await clone({
  fs,
  http,
  dir,
  url
})
```

At this point there is an IndexedDB store called 'virtualfs' that stores a csvs dataste at '/csvs-template-en'. Let's point the dir variable there.
```js
const dir = "/csvs-template-en"
```

Now query the dataset 
```js
const CSVS = await import('@fetsorn/csvs-js');

const entries = await new CSVS({ fs, dir }).select(new URLSearchParams());
// [{
//   _: 'datum',
//   UUID: '...',
//   datum: 'value1',
//   actdate: '2001-01-01'
// }]
```

Check out more uses of csvs-lib in [evenor](https://gitlab.com/norcivilian-labs/evenor) and [csvs-nodejs](https://gitlab.com/norcivilian-labs/csvs-nodejs).
