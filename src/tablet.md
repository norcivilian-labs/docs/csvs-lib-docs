# tablet

tablet is a function that accepts the record, tablet, fs, dir and returns the record and IO.

 - shell: record -> Tablet -> fs -> dir -> IO record
 - shell: JSON -> Tablet -> FS -> String -> IO JSON

```pdl
if select
    if accumulating
        if has record
            forward
        else
            assign initial record
            assign current record
            assign match map
            for each line of filename
                parse line
                if record complete 
                    if new match
                        enqueue record
                        set match
                    delete record
            parse line
            if record complete TODO: merge this with previous
              if new match
                  enqueue record
                  set match
              delete record
            enqueue record and match map
    else
        for each line of filename
            parse line
                if record complete 
                    enqueue record
                    set match
                    delete record
        parse line TODO: merge this with previous
            if record complete 
                enqueue record
                set match
                delete record
        if passthrough and no match
            enqueue record
else if update
    for each line of filename
        parse line
        if line complete
            enqueue line
        assign fst
        assign isMatch
    parse line TODO: merge this with previous
    if line complete
        enqueue line
    for each line in queue
        append line to tmp
    write tmp to filename
else if delete
    for each line of filename
        parse line
        if no match
            enqueue line
    for each line in queue
        append line to tmp
    write tmp to filename
else if insert
    for each relation in record
        enqueue line
    append line to filename
else
    panic
```
