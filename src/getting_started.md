# Getting Started

This documentation describes the library for searching and updating [CSVS](https://csvs-format.docs.norcivilianlabs.org/) datasets.

Install from the NPM registry
```shell
yarn install @fetsorn/csvs-js
```

Import the library
```js
import { CSVS } from @fetsorn/csvs-js;
```

Clone a sample csvs dataset
```shell
git clone https://gitlab.com/norcivilian-labs/csvs-template-en
```

Point the `dir` variable to the dataset. In NodeJS, that would be the path to a dataset directory.
```js
import fs from 'fs'; 

const dir = "/path/to/csvs-template-en"

const query = new CSVS({ fs, dir });
```

And search the records
```js
const records = await query.select(new URLSearchParams());
// [{
//   _: 'datum',
//   UUID: '...',
//   datum: 'value1',
//   actdate: '2001-01-01'
// }]
```

Next, learn more about csvs integration in the [Tutorial](./tutorial.md) and the [User Guides](./user_guides.md).
